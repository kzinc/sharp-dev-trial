import React, { Component, ReactElement } from 'react';
import {
  StyleSheet,
  SafeAreaView,
} from 'react-native';
import AuthScreen from './components/AuthScreen/AuthScreen';
import Header from './components/Header/Header';
import NewTransfer, { SelectedUsertype } from './components/NewTransfer/NewTransfer';
import Spinner from './components/Spinner/Spinner';
import TransferList from './components/TransferList/TransferList';
import CustomAlert from './helpers/customAlert';
import {
  getLoggedUser, getLoggedUserTransactions, sendPW, UserInterface,
} from './helpers/getDataRequest.helper';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

const mockUser: UserInterface = {
  balance: 0,
  email: '',
  id: 0,
  name: '',
};

class App extends Component {
  state = {
    loading: false,
    token: '',
    user: mockUser,
    userTransactions: [],
  };

  async handleSaveToken(newToken: string): Promise<void> {
    const loggedUser = await getLoggedUser(newToken);
    const transactionsList = await getLoggedUserTransactions(newToken);
    this.setState({
      user: loggedUser.success ? loggedUser.success.user_info_token : undefined,
      userTransactions: transactionsList.success ? transactionsList.success.trans_token : [],
      token: newToken,
    });
  }

  handleTransaction(recipient: SelectedUsertype | undefined, amount: number): void {
    const { token, user } = this.state;
    if (!recipient) {
      CustomAlert('Sorry, something is wrong with the recipient');
    } else if (amount < user.balance) {
      sendPW(recipient.name, amount, token).then(() => {
        this.handleSaveToken(token).then();
      });
    } else {
      CustomAlert('Sorry, you do not have enough PW for this transaction');
    }
  }

  handleLogout(): void {
    this.setState({ token: '' });
  }

  render(): ReactElement {
    const {
      loading, token, user, userTransactions,
    } = this.state;
    if (loading) return <Spinner/>;
    if (!token) {
      return (
        <AuthScreen getToken={(newToken: string) => this.handleSaveToken(newToken)}/>
      );
    }
    return (
      <SafeAreaView style={styles.container}>
        <Header logout={() => this.handleLogout()} user={user}/>
        <NewTransfer
          token={token}
          onTransaction={(recipient, amount) => this.handleTransaction(recipient, amount)}
        />
        <TransferList userTransactions={userTransactions}/>
      </SafeAreaView>
    );
  }
}

export default App;
