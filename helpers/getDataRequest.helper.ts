/* eslint-disable camelcase,@typescript-eslint/camelcase */
import CustomAlert from './customAlert';

const server = 'http://193.124.114.46:3001';

export interface UserInterface {
  balance: number,
  email: string,
  id: number,
  name: string,
}
export interface UserInfoResponse{
  error?: string,
  success?:{
    user_info_token: UserInterface,
  }
}

export interface GetUsersResponse {
  error?: string,
  success?:{
    id: string,
    name: string
  }[]
}
export interface LoginResponseInterface {
  error?: string,
  success?: {
    id_token: string,
  },
}

export interface TransactionInterface {
  amount: number,
  balance: number,
  date: string,
  id: string,
  username: string
}

export interface UserTransactionsListInterface {
  error?: string,
  success?: {
    trans_token: TransactionInterface[]
  },
}

const post = async (url: string, token = '', body: any): Promise<any> => {
  const response = await fetch(`${server}${url}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json;charset=utf-8',
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify(body),
  });
  if (response.ok) {
    const success = await response.json();
    return { success };
  }
  const error = await response.text();
  CustomAlert(error);
  return { error };
};

const get = async (url: string, token = ''): Promise<any> => {
  const response = await fetch(`${server}${url}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json;charset=utf-8',
      Authorization: `Bearer ${token}`,
    },
  });
  if (response.ok) {
    const success = await response.json();
    return { success };
  }
  const error = await response.text();
  CustomAlert(error);
  return { error };
};

export const getLoggedUser = async (token: string): Promise<UserInfoResponse> => get('/api/protected/user-info', token);

export const getLoggedUserTransactions = async (token: string): Promise<UserTransactionsListInterface> => get('/api/protected/transactions', token);

export const login = async (email: string, password: string): Promise<LoginResponseInterface> => post('/sessions/create', '', {
  email,
  password: +password,
});
export const sendPW = async (name: string, amount: number, token: string): Promise<LoginResponseInterface> => post('/api/protected/transactions', token, {
  name,
  amount,
});

export const getFilteredUsersList = async (filter: string, token: string): Promise<GetUsersResponse> => post('/api/protected/users/list', token, {
  filter,
});

export const register = async (username: string, password: string, email: string): Promise<LoginResponseInterface> => post('/users', '', {
  username,
  password,
  email,
});

export default login;
