import { Alert } from 'react-native';

const CustomAlert = (text: string) => {
  Alert.alert(
    'Sorry, but something is not right',
    text,
    [
      { text: 'OK' },
    ],
    { cancelable: true },
  );
};

export default CustomAlert;
