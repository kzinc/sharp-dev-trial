import React, { ReactElement } from 'react';
import { View, StyleSheet } from 'react-native';
import Preloader from 'react-native-spinkit';
import Colors from '../../helpers/assets/Colors';

const styles = StyleSheet.create({
  spinnerContainer: {
    flex: 1,
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  spinner: {
    height: 300,
    width: 300,
  },
});

const Spinner = (): ReactElement => (
  <View style={styles.spinnerContainer}>
    <Preloader
      style={styles.spinner}
      isVisible
      size={36}
      type="ChasingDots"
      color={Colors.borderAccent}
    />
  </View>
);

export default Spinner;
