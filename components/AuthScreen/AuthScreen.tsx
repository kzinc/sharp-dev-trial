import React, { ReactElement, useState } from 'react';
import {
  Alert,
  Button, ScrollView, StyleSheet, Text, TextInput, View,
} from 'react-native';
import Colors from '../../helpers/assets/Colors';
import CustomAlert from '../../helpers/customAlert';
import login, { register } from '../../helpers/getDataRequest.helper';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: '100%',
  },
  topButtonContainer: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'flex-end',
    marginRight: 20,
    paddingRight: 20,
    paddingTop: 20,
  },
  bottomButtonContainer: {
    flexDirection: 'row',
    width: '100%',
    marginTop: 50,
    marginBottom: 20,
    justifyContent: 'center',
  },
  input: {
    height: 40,
    borderColor: Colors.border,
    borderWidth: 1,
    marginVertical: 10,
    marginHorizontal: 10,
  },
});

const buttons = {
  logIn: {
    title: 'Log in',
    toScreen: 'logIn',
  },
  signIn: {
    title: 'Sign in',
    toScreen: 'signIn',
  },
};

interface InputsInterface {
  logIn: string[],
  signIn: string[],
}

const inputs: InputsInterface = {
  logIn: ['email', 'password'],
  signIn: ['name', 'email', 'password', 'passwordAgain'],
};

const initialFields = {
  name: '',
  email: '',
  password: '',
  passwordAgain: '',
};

type fieldsType = 'name' | 'email' | 'password'| 'passwordAgain';

const AuthScreen = ({ getToken } : {getToken: (newToken: string) => void}): ReactElement => {
  const [screen, setScreen] = useState('logIn');
  const [fields, setFields] = useState(initialFields);

  const handleScreenChange = (newScreen: 'logIn' | 'signIn') => {
    if (newScreen !== screen) setScreen(newScreen);
  };

  const authorise = (token: string): void => {
    getToken(token);
  };

  const handleButtonAction = (): boolean => {
    const regEmail = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
    const regName = /([a-zA-Zа-яА-я])/;
    if (!regEmail.test(fields.email)) {
      CustomAlert('It seems like your email is not correct');
      return false;
    }
    if (!fields.password) {
      CustomAlert('It looks like you forgot to type your password');
      return false;
    }
    if (screen === 'signIn' && fields.password !== fields.passwordAgain) {
      CustomAlert('Please try to type both of your password and password confirmation equal');
      return false;
    }
    if (screen === 'signIn' && (!fields.name || !regName.test(fields.name))) {
      CustomAlert('Please try to use another name, may be more official one');
      return false;
    }
    if (screen === 'logIn') {
      login(fields.email, fields.password).then((result) => {
        if (result.success) authorise(result.success.id_token);
      });
      return true;
    }
    register(fields.name, fields.email, fields.password).then((result) => {
      if (result.success) authorise(result.success.id_token);
    });
    return true;
  };

  const onInputChangeText = (field: fieldsType, text: string) => {
    setFields((prevState) => ({ ...prevState, [field]: text }));
  };

  const getTextContentType = (field: string) => {
    if (field === 'email') return 'emailAddress';
    if (field === 'password' || field === 'passwordAgain') return 'password';
    return 'name';
  };

  const renderInputs = () => inputs[screen as 'logIn' | 'signIn'].map((field) => (
    <View key={field}>
      <Text>{field}</Text>
      <TextInput
        style={styles.input}
        textContentType={getTextContentType(field)}
        onChangeText={(text) => onInputChangeText(field as fieldsType, text)}
        value={fields[field as fieldsType]}
      />
    </View>
  ));

  const renderTopButton = () => {
    const { title, toScreen } = screen === 'logIn' ? buttons.signIn : buttons.logIn;
    return (
      <View style={styles.topButtonContainer}>
        <Button title={title} onPress={() => handleScreenChange(toScreen as 'logIn' | 'signIn')}/>
      </View>
    );
  };

  const renderBottomButton = () => {
    const { title } = buttons[screen as 'logIn' | 'signIn'];
    return (
      <View style={styles.bottomButtonContainer}>
        <Button title={title} onPress={() => handleButtonAction()}/>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      {renderTopButton()}
      <ScrollView>
        {renderInputs()}
      </ScrollView>
      {renderBottomButton()}
    </View>
  );
};

export default AuthScreen;
