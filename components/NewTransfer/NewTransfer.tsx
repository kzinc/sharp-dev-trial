/* eslint-disable react-native-a11y/has-accessibility-props */
import React, { ReactElement, useState } from 'react';
import {
  Button, FlatList, StyleSheet, Text, TextInput, TouchableOpacity, View,
} from 'react-native';
import Colors from '../../helpers/assets/Colors';
import { getFilteredUsersList } from '../../helpers/getDataRequest.helper';

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
  },
  transactionContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    marginHorizontal: 20,
  },
  buttonContainer: {
    marginLeft: 'auto',
    flexDirection: 'row',
  },
  input: {
    width: 180,
    borderRadius: 5,
    borderColor: Colors.borderAccent,
    borderWidth: 1,
  },
  inputNums: {
    width: 90,
    borderRadius: 5,
    borderColor: Colors.borderAccent,
    borderWidth: 1,
    marginLeft: 20,
  },
  selectItem: {
    margin: 10,
    padding: 5,
    borderBottomColor: Colors.border,
    borderBottomWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  selectItemText: {
    paddingHorizontal: 10,
  },
  selectedItemBig: {
    paddingHorizontal: 10,
    fontSize: 20,
    fontWeight: 'bold',
    color: '#45ef45',
  },
});

export type SelectedUsertype = {
  id: string,
  name: string
};

const NewTransfer = (
  {
    onTransaction,
    token,
  }: {
    onTransaction: (selectedUser: SelectedUsertype | undefined, amount: number) => void,
    token: string
  },
): ReactElement => {
  const [userList, setUserList] = useState([] as SelectedUsertype[]);
  const [selectedUser, selectUser] = useState(undefined as SelectedUsertype | undefined);
  const [requestTimeout, setRequestTimeout] = useState(0);
  const [amount, setAmount] = useState('');
  const [sendTo, setSendTo] = useState('');

  const onInputChangeText = (text: string): void => {
    if (text) {
      if (requestTimeout) clearTimeout(requestTimeout);
      const saveTimeout = setTimeout(() => {
        getFilteredUsersList(text, token).then((result) => {
          if (result && result.success) {
            setUserList(result.success);
          }
        });
      }, 3000);
      setRequestTimeout(saveTimeout);
    }
    setSendTo(text);
  };

  const renderListItem = (
    { item: { id, name } }: { item: SelectedUsertype },
  ): ReactElement => (
    <TouchableOpacity onPress={() => selectUser({ id, name })}>
      <View style={styles.selectItem}>
        <Text style={styles.selectItemText}>#{id}</Text>
        <Text style={styles.selectItemText}>{name}</Text>
      </View>
    </TouchableOpacity>
  );

  const renderUserList = (): ReactElement | null => {
    if (selectedUser) return null;
    return (
      <View>
        <FlatList
          data={userList}
          renderItem={renderListItem}
          keyExtractor={(item) => `${item.id}`}
        />
      </View>
    );
  };

  const renderRecipientBlock = (): ReactElement => {
    if (selectedUser) {
      return (
        <TouchableOpacity onPress={() => {
          selectUser(undefined);
          setSendTo(selectedUser.name);
        }}
        >
          <View>
            <Text style={styles.selectedItemBig}>{selectedUser.name}</Text>
          </View>
        </TouchableOpacity>

      );
    }

    return (
      <TextInput
        autoCompleteType="username"
        style={styles.input}
        placeholder="Payment recipient"
        onChangeText={(text) => onInputChangeText(text)}
        value={sendTo}
      />
    );
  };

  return (
    <View style={styles.container}>
      <Text> Create new transaction</Text>
      <View style={styles.transactionContainer}>
        {renderRecipientBlock()}
        <TextInput
          keyboardType="numeric"
          style={styles.inputNums}
          placeholder="How much?"
          onChangeText={(text) => setAmount(text)}
          value={amount}
        />
        <View style={styles.buttonContainer}>
          <Button title="Send" onPress={() => onTransaction(selectedUser, +amount)}/>
        </View>
      </View>
      {renderUserList()}
    </View>
  );
};

export default NewTransfer;
