import React, { ReactElement } from 'react';
import {
  FlatList,
  StyleSheet, Text, View,
} from 'react-native';
import { TransactionInterface } from '../../helpers/getDataRequest.helper';
import Colors from '../../helpers/assets/Colors';

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
  },
  selectItem: {
    margin: 10,
    padding: 5,
    borderBottomColor: Colors.border,
    borderBottomWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  selectItemText: {
    paddingHorizontal: 10,
  },
});

export type SelectedUsertype = {
  id: string,
  name: string
};

const TransferList = (
  { userTransactions }: {
    userTransactions: TransactionInterface[],
  },
): ReactElement => {
  const renderListItem = (
    {
      item: {
        username, date, amount, balance,
      },
    }: { item: TransactionInterface },
  ): ReactElement => (
    <View style={styles.selectItem}>
      <Text style={styles.selectItemText}>{date}</Text>
      <Text style={styles.selectItemText}>{username}</Text>
      <Text style={styles.selectItemText}>{amount}</Text>
      <Text style={styles.selectItemText}>{balance}</Text>
    </View>
  );

  return (
    <View style={styles.container}>
      <FlatList
        data={userTransactions}
        renderItem={renderListItem}
        keyExtractor={(item) => `${item.id}`}
      />
    </View>
  );
};

export default TransferList;
