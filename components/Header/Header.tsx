import React, { ReactElement } from 'react';
import {
  Button, StyleSheet, Text, View,
} from 'react-native';
import Colors from '../../helpers/assets/Colors';
import { UserInterface } from '../../helpers/getDataRequest.helper';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 50,
    borderColor: Colors.border,
    borderWidth: 2,
    marginVertical: 5,
  },
  name: {
    marginHorizontal: 20,
    fontSize: 20,
    color: Colors.borderAccent,
  },
  balance: {
    fontSize: 20,
    color: '#3ecf8d',
  },
  topButtonContainer: {
    marginLeft: 'auto',
    marginRight: 20,
  },
});

const Header = (
  { logout, user: { name, balance } }: { logout: () => void, user: UserInterface },
): ReactElement => (
  <View style={styles.container}>
    <Text style={styles.name}>{name}</Text>
    <Text style={styles.balance}>{balance} PW</Text>
    <View style={styles.topButtonContainer}>
      <Button title="Log out" onPress={() => logout()}/>
    </View>
  </View>
);

export default Header;
